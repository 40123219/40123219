#@+leo-ver=5-thin
#@+node:cmsxh.20140107172103.1828: * @file brython2.py
#coding: utf-8


#@@language python
#@@tabwidth -4

#@+<<declarations>>
#@+node:cmsxh.20140107172103.1829: ** <<declarations>> (brython)
import cherrypy
import os
import sys

# 確定程式檔案所在目錄, 在 Windows 有最後的反斜線
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    sys.path.append(os.path.join(os.getenv("OPENSHIFT_REPO_DIR"), "wsgi"))
else:
    sys.path.append(_curdir)
#@-<<declarations>>
#@+others
#@+node:cmsxh.20140107172103.1830: ** class Brython
class Brython(object):
    '''
    用來導入 Brython 網際運算環境
    '''
    #@+others
    #@+node:cmsxh.20140107172103.1831: *3* index
    @cherrypy.expose
    def index(self):
        return '''
    <a href="/">回到首頁</a><br />
    <script type="text/javascript" src="/static/Brython1.2-20131109-201900/brython.js"></script>
    <script type="text/javascript">
    window.onload = function(){
        brython(1);
    }
    </script><script type="text/python">
    import sys
    import time
    import dis

    if sys.has_local_storage:
        from local_storage import storage
    else:
        storage = False

    def reset_src():
        if storage:
            doc['src'].value = storage["py_src"]

    def write(data):
        doc["console2"].value += str(data)

    #sys.stdout = object()    #not needed when importing sys via src/Lib/sys.py
    sys.stdout.write = write

    def to_str(xx):
        return str(xx)

    doc['version'].text = '.'.join(map(to_str,sys.version_info))

    # 配合 20130817 版本, 改為下列流程
    def write(data):
        doc["console2"].value += str(data)
    sys.stdout.write = write
    sys.stderr.write = write
    # 結束 20130817 版本修改

    output = ''

    def show_console2():
        doc["console2"].value = output
        doc["console2"].cols = 60

    def clear_text():
        log("event clear")
        doc['console2'].value=''
        #doc['src'].value=''

    def clear_canvas():
        canvas = doc["plotarea"]
        ctx = canvas.getContext("2d")
        ctx.clearRect(0, 0, canvas.width, canvas.height)

    def run():
        global output
        doc["console2"].value=''
        doc["console2"].cols = 60
        src = doc["src"].value
        if storage:
            storage["py_src"]=src
        t0 = time.time()
        exec(src)
        output = doc["console2"].value
        print('<done in %s ms>' %(time.time()-t0))

    def show_js():
        src = doc["src"].value
        doc["console2"].cols = 90
        doc["console2"].value = dis.dis(src)

    </script>
    <table width=80%>
    <tr><td style="text-align:center"><b>Python</b>
    </td>
    <td>&nbsp;</td>
    <th><input type="button" value="Console" onClick="show_console2()"></th>
    <th><input type="button" value="Javascript" onClick="show_js()"></th>
    </tr>
    <tr><td><div id="editor"></div><textarea id="src" name=form_34ed066df378efacc9b924ec161e7639program cols="60" rows="20">
    #coding: utf-8
    # 猜數字遊戲
    import random
     
    標準答案 = random.randint(1, 100)
    你猜的數字 = int(input(&quot;請輸入您所猜的整數:&quot;))
    猜測次數 = 0
    while 標準答案 != 你猜的數字:
        if 標準答案 &lt; 你猜的數字:
            print(&quot;太大了，再猜一次 :)加油&quot;)
        else:
            print(&quot;太小了，再猜一次 :)加油&quot;)
        你猜的數字 = int(input(&quot;請輸入您所猜的整數:&quot;))
        猜測次數 += 1
     
    print(&quot;猜對了！總共猜了&quot;, 猜測次數, &quot;次&quot;)</textarea></td><td><input type="button" value="Run" onClick="run()"></td>
    <td><input type="button" value="Clear Output" onClick="clear_text()">
    <input type="button" value="Clear Canvas" onClick="clear_canvas()">
    </td>
    <td colspan=2><textarea id="console2" cols="60" rows="20"></textarea></td>
    </tr>
    <tr><td colspan=2>
    <p>Brython version <span id="version"></td>
    </tr>
    <tr><td colspan="4">
    <div id="outputdiv"></div>
    <textarea id="dataarea" cols=30 rows=5></textarea>
    </td>
    </tr>
    <tr><td colspan="4">
    <canvas id="plotarea" width="640" height="640"></canvas>
    </td>
    </tr>
    </table>
    '''
    #@-others
#@-others
application_conf = {'/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"}
    }
    
if __name__ == '__main__':
    # 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
    if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
        # 雲端執行啟動
        application = cherrypy.Application(Brython(), config = application_conf)
    else:
        # 近端執行啟動
        '''
        cherrypy.server.socket_port = 8083
        cherrypy.server.socket_host = '127.0.0.1'
        '''
        cherrypy.quickstart(Brython(), config = application_conf)
#@-leo
